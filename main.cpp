/*
    Copyright (C) 2016 TurningTechnologies, LLC.
    Contact: https://www.turningtechnologies.com/about-turning-technologies/contact-information

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtWidgets>
#include <QtConcurrent>
#include <QFile>
#include <sys/time.h>

using namespace QtConcurrent;

static int nTimeOut = 0;
static struct timeval tvTime;
static QString sFilePath;

int main(int argc, char **argv)
{
    try
    {
        QApplication app(argc, argv);
        QApplication::setApplicationName("Progress");
        QApplication::setApplicationVersion("1.0");

        QCommandLineParser parser;
        parser.setApplicationDescription("Indeterminate Progressbar");
        parser.addHelpOption();
        parser.addVersionOption();

        // Title
        QCommandLineOption titleOption(QStringList() << "T" << "title",
                                       QCoreApplication::translate("main", "Set the Window Title."),
                                       QCoreApplication::translate("main", "title"));
        titleOption.setDefaultValue("Progress");
        parser.addOption(titleOption);

        // Time-out
        QCommandLineOption timeoutOption(QStringList() << "t" << "timeout",
                                   QCoreApplication::translate("main", "Time-Out in seconds."),
                                   QCoreApplication::translate("main", "#"));
        timeoutOption.setDefaultValue("0");
        parser.addOption(timeoutOption);

        // Watch file
        QCommandLineOption watchFileOption(QStringList() << "w" << "watchfile",
                                     QCoreApplication::translate("main", "Run while a file exist."),
                                     QCoreApplication::translate("main", "file path"));
        watchFileOption.setDefaultValue("");
        parser.addOption(watchFileOption);

        // License
        QCommandLineOption showLicense("license", QCoreApplication::translate("main", "Display license information."));
        parser.addOption(showLicense);

        parser.process(app);

        if(parser.isSet(showLicense))
        {
            qInfo("");
            qInfo("Copyright (C) 2016 TurningTechnologies, LLC.");
            qInfo("Contact: https://www.turningtechnologies.com/about-turning-technologies/contact-information");
            qInfo("");
            qInfo("This program is free software: you can redistribute it and/or modify");
            qInfo("it under the terms of the GNU General Public License as published by");
            qInfo("the Free Software Foundation, either version 3 of the License, or");
            qInfo("(at your option) any later version.");
            qInfo("");
            qInfo("This program is distributed in the hope that it will be useful,");
            qInfo("but WITHOUT ANY WARRANTY; without even the implied warranty of");
            qInfo("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
            qInfo("GNU General Public License for more details.");
            qInfo("");
            qInfo("You should have received a copy of the GNU General Public License");
            qInfo("along with this program.  If not, see <http://www.gnu.org/licenses/>.");
            qInfo("");
        }
        else
        {
            // Set Global variables
            nTimeOut = parser.value(timeoutOption).toInt();
            sFilePath = parser.value(watchFileOption);
            gettimeofday(&tvTime, NULL);
            
            // Initialize the Progress Dialog
            QProgressDialog dialog;
            dialog.setWindowTitle(parser.value(titleOption));
            dialog.setAutoReset(true);
            dialog.setAutoClose(false);
            dialog.setMaximum(101);
            dialog.setMinimum(0);
            dialog.setValue(1);
            dialog.setWindowModality(Qt::WindowModal);
            dialog.repaint();
            // Infinate loop...
            while(true)
            {
                // check for time-out
                if(nTimeOut > 0)
                {
                    struct timeval tvCurrent;
                    gettimeofday(&tvCurrent, NULL);
                    if((tvCurrent.tv_sec - tvTime.tv_sec) > nTimeOut)
                    {
                        break;
                    }
                }
                // check for the wait file
                if(!sFilePath.isEmpty())
                {
                    QFile file(sFilePath);
                    if(!file.exists())
                    {
                        break;
                    }
                }
                // check for user cancele
                if(dialog.wasCanceled())
                {
                    break;
                }
                // increment the progress bar
                if(dialog.value() < 100)
                {
                    dialog.setValue(dialog.value() + 1);
                }
                else
                {
                    dialog.setValue(1);
                }
                QThread::msleep(25);
            }
        }
    }
    catch(...)
    {
        qCritical("caught error: Exiting...");
    }
}
