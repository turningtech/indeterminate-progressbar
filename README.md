# Progress

## A simple QT base Indeterminate Progressbar

This application was designed specifically to be uses as a standalone statically linked binary on Mac OSX.

If run without options it just display a dialog with an indeterminate progressbar until the user closes it.

### Options
* -h, --help                   Displays this help.
* -v, --version                Displays version information.
* -T, --title <title>          Set the Window Title.
* -t, --timeout <#>            Time-Out in seconds.
* -w, --watchfile <file path>  Run while a file exist.
* --license                    Display license information.

### Source
- Git: `https://bitbucket.org/turningtech/indeterminate-progressbar.git`


### Dependencies
- QT5 (only tested with staticly linked v5.6.0) http://download.qt.io/official_releases/qt/5.6/5.6.0/single/qt-everywhere-opensource-src-5.6.0.tar.gz

### Compile
* Build QT5 from source for Static Linking: http://doc.qt.io/qt-5/osx-deployment.html

 > ./configure -static -prefix $PWD/qtbase -opensource -nomake tests -release

 > make -j4

* Use qmake that was built to generate a Makefile, i.e. ~/qt-everywhere-opensource-src-5.6.0/qtbase/bin/qmake

 > qmake Progress.pro -r -spec macx-clang CONFIG+=release CONFIG+=x86_64 LIBS+=-dead_strip

 > make

### License
==============================================================================

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the
   Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.

   Or visit http://www.gnu.org/licenses/

==============================================================================